﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cyest.Carbon.Modeller.Server.Models;
using Cyest.Carbon.Technologies.Logic.Engine;
using Cyest.Carbon.Technologies.Logic.Engine.Commands;
using Cyest.Carbon.Modeller.Domain;
using System.Runtime.Serialization;
using Cyest.Carbon.Modeller.Server.Models.Dimensions;

namespace ModelObjectDump
{
    class Program
    {
        private static StreamWriter objectsFile;
        private static StreamWriter attributesFile;
        private static StreamWriter templateInstancesFile;
        private static StreamWriter templateParametersFile;
        

        public static void Main(string[] args)
        {
            if (!validateModels(args))
            {
                Environment.Exit(0);
            }

            for (int i=0; i<2; i++)
            {
                Dump(args[i], i);
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static bool validateModels(string[] args)
        {
            if (args == null)
            {
                Console.WriteLine("Please specify before and after models");
                return false;
            }         
            else if (args.Length < 2)
            {
                Console.WriteLine("Please specify after model");
                return false;
            }
            else if (args.Length > 2)
            {
                Console.WriteLine("Please specify only two models");
                return false;
            }

            return true;
        }

        public static void Dump(string modelPath, int selector)
        {
            Console.WriteLine(modelPath);
            string dumpFolder = "..\\..\\output\\before\\";
            
            if (selector == 1)
            {
                dumpFolder = "..\\..\\output\\after\\";
            }

            objectsFile = new StreamWriter(string.Concat(dumpFolder, "Objects.txt"));
            attributesFile = new StreamWriter(string.Concat(dumpFolder, "Attributes.txt"));
            templateInstancesFile = new StreamWriter(string.Concat(dumpFolder, "Template Instances.txt"));
            templateParametersFile = new StreamWriter(string.Concat(dumpFolder, "Template Parameters.txt"));  

            using (Model model = new Model(modelPath))
            {

                //var tbaseDescriptor = model.Template2Manager.GetTemplateModels().Single(x => x.Definition.Name == "T Base");
                //var tbasePath = model.Template2Manager.GetCopyOfModelForServer(tbaseDescriptor.Definition.TemplateId);
                //{
                //    using (var tbaseModel = new Model(tbasePath))
                //    {
                //        printObjects(model, tbaseModel.Workspace.RootObjects);

                //    }
                //}
                try
                {
                    printObjects(model, model.Workspace.RootObjects.OrderByDescending(x => x.UnmangledName));
                }
                finally 
                {
                    objectsFile.Close();
                    attributesFile.Close();
                    templateInstancesFile.Close();
                    templateParametersFile.Close();
                    Console.WriteLine("Dump complete");
                }

            }
        }

        private static void printObjects(Model model, IEnumerable<FinObject> objects)
        {                       
            foreach (var finObj in objects)
            {
                ObjectPath path = finObj.GetObjectPath(model);

                string objectType = model.Template2Manager.IsTemplateInstance(finObj) ? "<I>" : "<O>";
                string objectLine = "";
                string attributeLine = "";
                string templateParametersLine = "";

                objectLine += path.ToString();

                if (objectType == "<O>")
                {
                    // Check for attributes on this object
                    foreach (var attr in finObj.Attributes)
                    {
                        attributeLine = path.ToString();
                        attributeLine += string.Format("[{0}]", model.NameManager.UnmangleName(attr.Name));
                        attributesFile.WriteLine(attributeLine);
                    }

                    objectsFile.WriteLine(objectLine);    
                }

                if (objectType == "<I>")
                {
                    templateInstancesFile.WriteLine(objectLine);
                    
                    // Check for parameters on this template instance
                    var tempValues = model.Template2Manager.GetTemplateValues(finObj);

                    if (tempValues.ParameterValues.Any())
                    {
                        templateParametersFile.WriteLine(path);
                        templateParametersFile.WriteLine(new string('-', path.names[0].Length+2));
                        foreach (var entry in tempValues.ParameterValues)
                        {
                            templateParametersFile.WriteLine("{0} {1}", entry.Key.PadRight(30), entry.Value);
                        }
                        templateParametersFile.WriteLine();
                    }
                } 

                printObjects(model, finObj.Children.OrderByDescending(x => x.UnmangledName));
            }
        }


    }
}
